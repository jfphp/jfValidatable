��          �      �       0     1  ;   Q     �     �  "   �  "   �  +     +   7  1   c  1   �     �  $   �  0       5  0   S     �     �  '   �  $   �  $     %   &  5   L  2   �     �     �                       	             
                      Se requiere un valor para `{0}` Tipo `{2}` incorrecto para `{0}`. Tipo(s) permitido(s): {4} Valor incorrecto para `{0}` Valores inesperados: {0}{2} `{0}` debe ser mayor o igual a {4} `{0}` debe ser menor o igual a {4} `{0}` debe tener como máximo {4} elementos `{0}` debe tener como mínimo {4} elementos `{0}` debe tener una longitud mayor o igual a {4} `{0}` debe tener una longitud menor o igual a {4} `{0}` no puede ser nulo `{0}` no puede tener un valor vacío Project-Id-Version: jf/validatable 1.0.0
Report-Msgid-Bugs-To: Joaquín Fernández
PO-Revision-Date: 2024-03-05 17:14:09+0100
Last-Translator: Joaquín Fernández
Language-Team: Joaquín Fernández
Language: en_GB
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 A value is required for `{0}` Wrong type `{2}` for `{0}`. Allowed type(s): {4} Wrong value for `{0}` Unexpected values: {0}{2} `{0}` must be greater or equal than {4} `{0}` must be less or equal than {4} `{0}` must have at most {4} elements `{0}` must have at least {4} elements `{0}` must have a length greater than or equal to {4} `{0}` must have a length less than or equal to {4} `{0}` can not be null `{0}` can not be empty 