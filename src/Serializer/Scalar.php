<?php

namespace jf\Validatable\Serializer;

use jf\Serializer\Scalar as ScalarSerializer;

/**
 * Serializa un objeto dejando solamente los valores escalares o los listados de escalares.
 */
class Scalar extends Validatable
{
    /**
     * @inheritdoc
     */
    public function isValid(mixed $value) : bool
    {
        return parent::isValid($value) && (is_array($value) ? ScalarSerializer::class::isScalarList($value) : is_scalar($value));
    }
}