<?php

namespace jf\Validatable\Serializer;

use jf\Base\IId;
use jf\Serializer\Serializer;
use jf\Validatable\IValidatable;
use jf\Validatable\TValidatable;

/**
 * Serializador para objetos que implementan la interfaz `jf\Validatable\IValidatable`.
 */
class Validatable extends Serializer
{
    use TValidatable;

    /**
     * @inheritdoc
     */
    protected function getValue(object $item, string $property, array $config) : mixed
    {
        return $item instanceof IId && $property === 'id'
            ? $item->getId()
            : $item->$property;
    }

    /**
     * Devuelve las propiedades del elemento que serán serializadas con sus respectivos valores.
     *
     * @param IValidatable $item Elemento a serializar.
     *
     * @return array
     */
    protected function getValues(IValidatable $item) : array
    {
        $properties   = [];
        $validatables = $this->getValidatables($item);
        foreach ($validatables as $property => $config)
        {
            if ($this->isSerializable($property, $config))
            {
                $properties[ $property ] = $this->getValue($item, $property, $config);
            }
        }

        return $properties;
    }

    /**
     * @inheritdoc
     */
    public function isSerializable(string $property, array $config = []) : bool
    {
        // Las propiedades privadas y protegidas incluyen un byte nulo en sus nombres
        return !str_contains($property, "\0") && ($config[ $this->subkey ] ?? TRUE) === TRUE;
    }

    /**
     * @inheritdoc
     */
    public function serialize(mixed $value, string $id = '') : mixed
    {
        return parent::serialize(
            $value instanceof IValidatable
                ? $this->getValues($value)
                : $value,
            $id
        );
    }
}