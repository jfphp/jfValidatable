<?php

namespace jf\Validatable\Serializer;

use jf\Serializer\Properties;
use jf\Validatable\IValidatable;

/**
 * Serializador que toma en cuenta la existencia de getters para las propiedades protegidas.
 *
 * AVISO: Este serializador NO es recomendable para entidades, colecciones o cualquier otro
 *        elemento cuyos getters van a obtener valores de los repositorios ya que podría tener
 *        efectos colaterales y además la instancia construida será descartada por ser un objeto.
 */
class Getters extends Validatable
{
    /**
     * Espacios de nombres de los tipos a ignorar.
     * Por defecto omitmos las entidades para evitar tener que hacer consultas innecesarias.
     *
     * @var string[]
     */
    public array $ignoreTypes = [];

    /**
     * @inheritdoc
     */
    protected function getKey(string $parent, string $id) : string
    {
        return $id ? preg_replace('/\W+/', '', $id) : $id;
    }

    /**
     * @inheritdoc
     */
    protected function getValue(object $item, string $property, array $config) : mixed
    {
        $getter = lcfirst(self::pascalize($this->getKey('', $property)));
        if (method_exists($item, $getter))
        {
            // Uso de $dto->name() para los DTOs
            $value = $item->$getter();
        }
        else
        {
            // Uso de $obj->getName() para los objetos que pueden tener un método setName
            $getter = 'get' . ucfirst($getter);
            $value  = method_exists($item, $getter)
                ? $item->$getter()
                : parent::getValue($item, $property, $config);
        }
        if ($value && is_object($value) && !$value instanceof IValidatable && is_iterable($value))
        {
            $values = [];
            foreach ($value as $k => $v)
            {
                if (is_object($v))
                {
                    $values[ $k ] = $v instanceof IValidatable
                        ? $this->getValues($v)
                        : (new Properties())->serialize($v);
                }
                else
                {
                    $values[ $k ] = $v;
                }
            }
            $value = $values;
        }

        return $value;
    }

    /**
     * @inheritdoc
     */
    public function isSerializable(string $property, array $config = []) : bool
    {
        $is = parent::isSerializable($property, $config);
        if ($is && $this->ignoreTypes)
        {
            $type = $config['type'] ?? NULL;
            if ($type && is_string($type))
            {
                foreach ($this->ignoreTypes as $ns)
                {
                    if (str_contains($type, $ns))
                    {
                        $is = FALSE;
                        break;
                    }
                }
            }
        }

        return $is;
    }
}