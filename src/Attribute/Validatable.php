<?php

namespace jf\Validatable\Attribute;

use Attribute;

/**
 * Atributo para marcar las propiedades validables.
 */
#[Attribute(Attribute::TARGET_PROPERTY)]
class Validatable
{
    /**
     * Constructor de la clase.
     *
     * @param array $config Configuración de validación.
     */
    public function __construct(array $config = [])
    {
    }
}