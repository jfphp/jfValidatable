<?php

namespace jf\Validatable;

/**
 * Interfaz para la validación de propiedades de clases y sus valores.
 */
interface IValidatable
{
    /**
     * Devuelve la información para la validar las propiedades de la clase.
     *
     * @return array<string,array<string,scalar>>
     */
    public static function validatables() : array;
}