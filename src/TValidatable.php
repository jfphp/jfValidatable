<?php

namespace jf\Validatable;

/**
 * Gestiona la obtención de la configuración de las propiedades validables.
 */
trait TValidatable
{
    /**
     * Clave de la configuración a aplicar sobre la configuración principal.
     *
     * Esto permite tener una configuración genérica y luego varias más específicas
     * según lo que se quiera validar (api, db, etc.).
     *
     * @var string
     */
    public string $key = '';

    /**
     * Permite modificar las propiedades validables de los objetos o asignar
     * validaciones a aquellos objetos que no implementan IValidatable.
     *
     * @var array<class-string,array>
     */
    public array $overrides = [];

    /**
     * Subclave a usar cuando la configuración recibida es un valor booleano.
     * Su principal uso es poder simplificar la configuración usando un valor booleano en vez
     * de un array y decidir a posteriori el uso que se le quiere dar.
     *
     * Por ejemplo, se puede tener una configuración para la lectura de la base de datos y otra
     * para la escritura asignando `$key = database` y `$subkey = read` o `$subkey = write`, en vez
     * de tener como configuración `['field' => ['database' => ['read' => true, 'write' => true]]]`
     * se puede tener `['field' => ['database' => true]]`.
     *
     * @var string
     */
    public string $subkey = 'read';

    /**
     * Obtiene la configuración de las propiedades validables del elemento.
     *
     * @param array|object|string $item Elemento a validar.
     *
     * @return array
     */
    public function getValidatables(array|object|string $item) : array
    {
        if (is_array($item))
        {
            $overrides    = NULL;
            $validatables = $item;
        }
        else
        {
            $overrides    = $this->overrides[ is_object($item) ? $item::class : $item ] ?? NULL;
            $validatables = $item instanceof IValidatable || (is_string($item) && is_a($item, IValidatable::class, TRUE))
                ? $item::validatables()
                : [];
        }
        $key    = $this->key;
        $subkey = $this->subkey;
        foreach ($validatables as &$config)
        {
            if ($config === NULL || is_scalar($config))
            {
                if ($config === TRUE)
                {
                    $config = [ $subkey => TRUE ];
                }
                elseif ($config && is_string($config))
                {
                    $config = [ $subkey => TRUE, 'type' => $config ];
                }
                else
                {
                    $config = [ $subkey => FALSE ];
                }
            }
            if ($key && array_key_exists($key, $config))
            {
                $value = $config[ $key ];
                unset($config[ $key ]);
                if (is_array($value))
                {
                    $config = array_replace_recursive($config, $value);
                }
                else
                {
                    $config[ $subkey ] = $value;
                }
            }
        }

        return $overrides
            ? array_replace_recursive($validatables, $overrides)
            : $validatables;
    }
}