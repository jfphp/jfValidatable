<?php

namespace jf\Validatable\Validator;

use BackedEnum;
use DateTimeImmutable;
use DateTimeInterface;
use jf\Base\AAssign;
use jf\Base\IAssign;
use jf\Collection\ACollection;
use jf\Collection\Objects;
use jf\Validatable\Assert;
use jf\Validatable\IValidatable;
use jf\Validatable\TValidatable;
use Throwable;
use UnitEnum;

/**
 * Valida las propiedadesde un objeto que requieren validación.
 */
class Validator extends AAssign implements IValidator
{
    use TValidatable;

    /**
     * Determina si una clase extiende de otra o implementa una interfaz.
     *
     * @param object|string $value     Valor a verificar.
     * @param string        $classname Clase o interfaz de la que se debe heredar.
     *
     * @return bool
     */
    protected function isA(object|string $value, string $classname) : bool
    {
        return class_exists($value) &&
               (class_exists($classname) || interface_exists($classname)) &&
               is_a($value, $classname, is_string($value));
    }

    /**
     * @inheritdoc
     */
    public function validate(array $values, array $config, string $id = '') : void
    {
        if ($values && $config)
        {
            Assert::noAdditionalKeys($values, $config, $id);
            $config = $this->getValidatables($config);
            if ($id && $id[ -1 ] !== '.')
            {
                $id .= '.';
            }
            foreach ($config as $name => $cfg)
            {
                $idname = $id . $name;
                if ($cfg['required'] ?? FALSE)
                {
                    Assert::arrayHasKey($name, $values, $idname);
                }
                if (array_key_exists($name, $values))
                {
                    $this->validateValue($values[ $name ], $cfg, $idname);
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function validateArray(array $value, array $config, string $id = '') : void
    {
        Assert::notEmptyValue($value, $config, $id);
        Assert::arrayMaxLength($value, $config, $id);
        Assert::arrayMinLength($value, $config, $id);
        if ($value)
        {
            if ($id && $id[ -1 ] !== '.')
            {
                $id .= '.';
            }
            $type           = $config['type'];
            $config['type'] = substr($type, 0, strpos($type, '['));
            foreach ($value as $k => $v)
            {
                $this->validateValue($v, $config, $id . $k);
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function validateBoolean(bool $value, array $config, string $id = '') : void
    {
    }

    /**
     * Valida los elementos de un colección.
     *
     *
     * @param ACollection|array         $values     Valores a validar.
     * @param class-string<ACollection> $collection Clase de la colección.
     * @param string                    $id         Nombre de la ruta siendo validada en el caso de llamadas recursivas.
     *
     * @return void
     */
    public function validateCollection(mixed $values, string $collection, string $id = '') : void
    {
        Assert::isCollection($values, $collection, $id);
        if (is_array($values) && is_a($collection, Objects::class, TRUE))
        {
            $ctype        = (new $collection())->classname;
            $validatables = $this->getValidatables($ctype);
            if ($validatables)
            {
                if ($id && $id[ -1 ] !== '.')
                {
                    $id .= '.';
                }
                foreach ($values as $key => $value)
                {
                    $key = $id . $key;
                    Assert::isCollectionItem($value, $ctype, $key);
                    if (is_array($value))
                    {
                        $this->validate($value, $validatables, $key);
                    }
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function validateDate(DateTimeInterface|string|int $value, array $config, string $id = '') : void
    {
        if (!$value instanceof DateTimeInterface)
        {
            try
            {
                $value = new DateTimeImmutable(is_int($value) ? date('c', $value) : $value);
            }
            catch (Throwable)
            {
                $value = NULL;
            }
        }
        Assert::isDateTimeInterface($value, $id);
    }

    /**
     * Valida un valor como si fuera un enumerado.
     *
     * @param UnitEnum|int|string $value  Valor a validar.
     * @param array               $config Configuración de la propiedad.
     * @param string              $id     Nombre de la propiedad.
     *
     * @return void
     */
    public function validateEnum(UnitEnum|int|string $value, array $config, string $id = '') : void
    {
        /** @var class-string<BackedEnum> $enum */
        $enum = $config['enum'];
        Assert::enumExists($enum);
        if (!$value instanceof $enum)
        {
            $value = $enum::tryFrom($value);
        }
        Assert::isEnumInstance($value, $enum, $id);
    }

    /**
     * @inheritdoc
     */
    public function validateFloat(float $value, array $config, string $id = '') : void
    {
        Assert::notEmptyValue($value, $config, $id);
        Assert::maxValue($value, $config, $id);
        Assert::minValue($value, $config, $id);
    }

    /**
     * @inheritdoc
     */
    public function validateInteger(int $value, array $config, string $id = '') : void
    {
        Assert::notEmptyValue($value, $config, $id);
        Assert::maxValue($value, $config, $id);
        Assert::minValue($value, $config, $id);
    }

    /**
     * @inheritdoc
     */
    public function validateNull(array $config, string $id = '') : void
    {
        Assert::isNullable($config, $id);
    }

    /**
     * @inheritdoc
     */
    public function validateObject(object $value, array $config, string $id = '') : void
    {
        Assert::isInstanceOfType($value, $config, $id);
    }

    /**
     * @inheritdoc
     */
    public function validateResource(mixed $value, array $config, string $id = '') : void
    {
    }

    /**
     * @inheritdoc
     */
    public function validateString(string $value, array $config, string $id = '') : void
    {
        Assert::notEmptyValue($value, $config, $id);
        Assert::strMaxLength($value, $config, $id);
        Assert::strMinLength($value, $config, $id);
    }

    /**
     * @inheritdoc
     */
    public function validateValue(mixed $value, array $config, string $id = '') : void
    {
        if ($value === NULL)
        {
            $this->validateNull($config, $id);
        }
        elseif (isset($config['enum']))
        {
            $this->validateEnum($value, $config, $id);
        }
        else
        {
            $ctype = $config['type'] ?? 'string';
            if ($ctype === 'datetime' || $this->isA($ctype, DateTimeInterface::class))
            {
                $this->validateDate($value, $config, $id);
            }
            elseif ($this->isA($ctype, IAssign::class) && !$this->isA($ctype, ACollection::class))
            {
                Assert::isAssignValue($value, $ctype, $id);
                if (is_array($value) && $this->isA($ctype, IValidatable::class))
                {
                    $this->validate($value, $this->getValidatables($ctype), $id);
                }
            }
            else
            {
                $collection = '';
                if ($ctype === 'scalar')
                {
                    $ctype = 'bool|float|int|string';
                }
                elseif ($ctype === 'json' || $ctype === 'mixed')
                {
                    $ctype = 'array|bool|float|int|string';
                }
                elseif ($ctype === 'float')
                {
                    $ctype = 'float|int';
                }
                elseif ($this->isA($ctype, ACollection::class))
                {
                    $collection = $ctype;
                    $ctype      .= '|array';
                }
                elseif (str_ends_with($ctype, '[]'))
                {
                    $ctype .= '|array';
                }
                $type = gettype($value);
                if ($type === 'boolean')
                {
                    $type = 'bool';
                }
                elseif ($type === 'double')
                {
                    $type = 'float';
                }
                elseif ($type === 'integer')
                {
                    $type = 'int';
                }
                Assert::allowedType($type, explode('|', $ctype), $id);
                if ($collection)
                {
                    $this->validateCollection($value, $collection, $id);
                }
                else
                {
                    $config['type'] = $ctype;
                    match ($type)
                    {
                        'array'    => $this->validateArray($value, $config, $id),
                        'bool'     => $this->validateBoolean($value, $config, $id),
                        'float'    => $this->validateFloat($value, $config, $id),
                        'int'      => $this->validateInteger($value, $config, $id),
                        'object'   => $this->validateObject($value, $config, $id),
                        'resource' => $this->validateResource($value, $config, $id),
                        'string'   => $this->validateString($value, $config, $id)
                    };
                }
            }
        }
    }
}