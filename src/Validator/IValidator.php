<?php

namespace jf\Validatable\Validator;

use DateTimeInterface;
use UnitEnum;

/**
 * Interfaz para la validación de valores.
 */
interface IValidator
{
    /**
     * Valida un listado de valores.
     *
     * @param mixed  $values Valores a validar.
     * @param array  $config Configuración de los elementos a validar.
     * @param string $id     Identificador del valor que se va a validar.
     *
     * @return void
     */
    public function validate(array $values, array $config, string $id = '') : void;

    /**
     * Valida un array.
     *
     * @param mixed  $value  Valor a validar.
     * @param array  $config Configuración del elemento a validar.
     * @param string $id     Identificador del valor que se va a validar.
     *
     * @return void
     */
    public function validateArray(array $value, array $config, string $id = '') : void;

    /**
     * Valida un valor booleano.
     *
     * @param mixed  $value  Valor a validar.
     * @param array  $config Configuración del elemento a validar.
     * @param string $id     Identificador del valor que se va a validar.
     *
     * @return void
     */
    public function validateBoolean(bool $value, array $config, string $id = '') : void;

    /**
     * Valida una fecha.
     *
     * @param DateTimeInterface|string|int $value  Valor a validar.
     * @param array                        $config Configuración del elemento a validar.
     * @param string                       $id     Identificador del valor que se va a validar.
     *
     * @return void
     */
    public function validateDate(DateTimeInterface|string|int $value, array $config, string $id = '') : void;

    /**
     * Valida una fecha.
     *
     * @param UnitEnum|string|int $value  Valor a validar.
     * @param array               $config Configuración del elemento a validar.
     * @param string              $id     Identificador del valor que se va a validar.
     *
     * @return void
     */
    public function validateEnum(UnitEnum|string|int $value, array $config, string $id = '') : void;

    /**
     * Valida un valor de coma flotante.
     *
     * @param float  $value  Valor a validar.
     * @param array  $config Configuración del elemento a validar.
     * @param string $id     Identificador del valor que se va a validar.
     *
     * @return void
     */
    public function validateFloat(float $value, array $config, string $id = '') : void;

    /**
     * Valida un valor entero.
     *
     * @param int    $value  Valor a validar.
     * @param array  $config Configuración del elemento a validar.
     * @param string $id     Identificador del valor que se va a validar.
     *
     * @return void
     */
    public function validateInteger(int $value, array $config, string $id = '') : void;

    /**
     * Valida un valor nulo.
     *
     * @param string $id     Identificador del valor que se va a validar.
     * @param array  $config Configuración del elemento a validar.
     *
     * @return mixed
     */
    public function validateNull(array $config, string $id = '') : void;

    /**
     * Valida un objeto.
     *
     * @param object $value  Objeto a validar.
     * @param array  $config Configuración del elemento a validar.
     * @param string $id     Identificador del valor que se va a validar.
     *
     * @return void
     */
    public function validateObject(object $value, array $config, string $id = '') : void;

    /**
     * Valida un recurso.
     *
     * @param resource $value  Valor a validar.
     * @param array    $config Configuración del elemento a validar.
     * @param string   $id     Identificador del valor que se va a validar.
     *
     * @return void
     */
    public function validateResource(mixed $value, array $config, string $id = '') : void;

    /**
     * Valida un texto.
     *
     * @param string $value  Valor a validar.
     * @param array  $config Configuración del elemento a validar.
     * @param string $id     Identificador del valor que se va a validar.
     *
     * @return void
     */
    public function validateString(string $value, array $config, string $id = '') : void;

    /**
     * Valida el valor llamando a los métodos `validate*` que sean necesarios.
     *
     * @param mixed  $value  Valor a validar.
     * @param array  $config Configuración del elemento a validar.
     * @param string $id     Identificador del valor que se va a validar.
     *
     * @return void
     */
    public function validateValue(mixed $value, array $config, string $id = '') : void;
}