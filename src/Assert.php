<?php

namespace jf\Validatable;

use DateTimeInterface;
use jf\assert\Assert as jfAssert;
use jf\Base\IAssign;
use jf\Collection\ACollection;

/**
 * Aserciones para la validación de los valores.
 */
class Assert extends jfAssert
{
    /**
     * Verifica que el tipo de datos esté entre los permitidos.
     *
     * @param string $type  Tipo a validar.
     * @param array  $types Tipos permitidos.
     * @param string $id    Identificador del valor que se va a validar.
     *
     * @return void
     */
    public static function allowedType(string $type, array $types, string $id) : void
    {
        static::inArray(
            $type,
            $types,
            FALSE,
            dgettext('validatable', 'Tipo `{2}` incorrecto para `{0}`. Tipo(s) permitido(s): {4}'),
            $id
        );
    }

    /**
     * Verifica que el array contenga la clave especificada.
     *
     * @param int|string $key   Clave que debe existir en el array.
     * @param array      $array Array que debe contener la clave.
     * @param string     $id    Identificador del valor que se va a validar.
     *
     * @return void
     */
    public static function arrayHasKey(int|string $key, array $array, string $id) : void
    {
        static::arrayKeyExists($key, $array, dgettext('validatable', 'Se requiere un valor para `{0}`'), $id);
    }

    /**
     * Verifica que el array no sobrepase la máxima longitud.
     *
     * @param array  $values Valores a validar.
     * @param array  $config Configuración de validación
     * @param string $id     Identificador del valor que se va a validar.
     *
     * @return void
     */
    public static function arrayMaxLength(array $values, array $config, string $id) : void
    {
        if (array_key_exists('maxLength', $config))
        {
            static::arrayCountLessOrEqual(
                $values,
                $config['maxLength'],
                dgettext('validatable', '`{0}` debe tener como máximo {4} elementos'),
                $id
            );
        }
    }

    /**
     * Verifica que el array tenga el número mínimo de elementos.
     *
     * @param array  $values Valores a validar.
     * @param array  $config Configuración de validación
     * @param string $id     Identificador del valor que se va a validar.
     *
     * @return void
     */
    public static function arrayMinLength(array $values, array $config, string $id) : void
    {
        if (array_key_exists('minLength', $config))
        {
            static::arrayCountGreaterOrEqual(
                $values,
                $config['minLength'],
                dgettext('validatable', '`{0}` debe tener como mínimo {4} elementos'),
                $id
            );
        }
    }

    /**
     * Verifica que el valor pueda ser aplicado a una instancia de la interfaz `IAssign`.
     *
     * @param mixed  $value Valor a validar.
     * @param string $class Clase que recibirá los valores.
     * @param string $id    Identificador del valor que se va a validar.
     *
     * @return void
     */
    public static function isAssignValue(mixed $value, string $class, string $id) : void
    {
        static::isTrue(
            $value instanceof $class || (is_a($class, IAssign::class, TRUE) && is_array($value)),
            dgettext('validatable', 'Valor incorrecto para `{0}`'),
            $id
        );
    }

    /**
     * Verifica que los valores sean una colección o puedan ser usados para crear una.
     *
     * @param mixed  $values     Valor a validar.
     * @param string $collection FQCN de la colección esperada.
     * @param string $id         Identificador del valor que se va a validar.
     *
     * @return void
     */
    public static function isCollection(mixed $values, string $collection, string $id) : void
    {
        static::isIterable($values, dgettext('validatable', 'Valor incorrecto para `{0}`'), $id);
        if (is_object($values))
        {
            static::isInstanceOf($values, ACollection::class, dgettext('validatable', 'Valor incorrecto para `{0}`'), $id);
            static::isInstanceOf($values, $collection, dgettext('validatable', 'Valor incorrecto para `{0}`'), $id);
        }
    }

    /**
     * Verifica que el valor sea un elemento de una colección o pueda ser construido a partir de dichos valores.
     *
     * @param mixed  $value Valor a validar.
     * @param string $class Clase del elemento.
     * @param string $id    Identificador del valor que se va a validar.
     *
     * @return void
     */
    public static function isCollectionItem(mixed $value, string $class, string $id) : void
    {
        static::isTrue($value instanceof $class || is_array($value), dgettext('validatable', 'Valor incorrecto para `{0}`'), $id);
    }

    /**
     * Verifica si el valor implementa la intefaz `DateTimeInterface`.
     *
     * @param mixed  $value Valor a validar.
     * @param string $id    Identificador del valor que se va a validar.
     *
     * @return void
     */
    public static function isDateTimeInterface(mixed $value, string $id) : void
    {
        static::isInstanceOf($value, DateTimeInterface::class, dgettext('validatable', 'Valor incorrecto para `{0}`'), $id);
    }

    /**
     * Verifica que el valor sea un enumerado de la clase especificada.
     *
     * @param mixed  $value Valor a validar.
     * @param string $enum  FQCN de la enumeración esperada.
     * @param string $id    Identificador del valor que se va a validar.
     *
     * @return void
     */
    public static function isEnumInstance(mixed $value, string $enum, string $id) : void
    {
        static::isInstanceOf($value, $enum, dgettext('validatable', 'Valor incorrecto para `{0}`'), $id);
    }

    /**
     * Verifica que el valor sea una instancia de los tipos especificados en la configuración.
     *
     * @param mixed  $value  Valor a validar.
     * @param array  $config Configuración de validación
     * @param string $id     Identificador del valor que se va a validar.
     *
     * @return void
     */
    public static function isInstanceOfType(mixed $value, array $config, string $id) : void
    {
        $found = FALSE;
        foreach (explode('|', $config['type'] ?? 'string') as $type)
        {
            if ($value instanceof $type)
            {
                $found = TRUE;
                break;
            }
        }
        static::isTrue($found, dgettext('validatable', 'Valor incorrecto para `{0}`'), $id);
    }

    /**
     * Verifica si el elemento puede ser nulo.
     *
     * @param array  $config Configuración de validación
     * @param string $id     Identificador del valor que se va a validar.
     *
     * @return void
     */
    public static function isNullable(array $config, string $id) : void
    {
        static::isTrue($config['nullable'] ?? TRUE, dgettext('validatable', '`{0}` no puede ser nulo'), $id);
    }

    /**
     * Verifica que el valor no sobrepase el máximo permitido.
     *
     * @param mixed  $value  Valor a validar.
     * @param array  $config Configuración de validación
     * @param string $id     Identificador del valor que se va a validar.
     *
     * @return void
     */
    public static function maxValue(mixed $value, array $config, string $id) : void
    {
        if (array_key_exists('maxValue', $config))
        {
            static::lessOrEqual($value, $config['maxValue'], dgettext('validatable', '`{0}` debe ser menor o igual a {4}'), $id);
        }
    }

    /**
     * Verifica que el valor sea mayor o igual al mínimo permitido.
     *
     * @param mixed  $value  Valor a validar.
     * @param array  $config Configuración de validación
     * @param string $id     Identificador del valor que se va a validar.
     *
     * @return void
     */
    public static function minValue(mixed $value, array $config, string $id) : void
    {
        if (array_key_exists('minValue', $config))
        {
            static::greaterOrEqual($value, $config['minValue'], dgettext('validatable', '`{0}` debe ser mayor o igual a {4}'), $id);
        }
    }

    /**
     * Verifica que un array no tenga claves adicionales a las configuradas.
     *
     * @param array  $input  Array con las claves a verificar.
     * @param array  $config Configuración de validación.
     * @param string $id     Identificador del valor que se va a validar.
     *
     * @return void
     */
    public static function noAdditionalKeys(array $input, array $config, string $id) : void
    {
        $keys = array_diff(array_keys($input), array_keys($config));
        static::empty($keys, dgettext('validatable', 'Valores inesperados: {0}{2}'), $id, array_values($keys));
    }

    /**
     * Verifica si un valor vacío es aceptable.
     *
     * @param mixed  $value  Valor a validar.
     * @param array  $config Configuración de validación
     * @param string $id     Identificador del valor que se va a validar.
     *
     * @return void
     */
    public static function notEmptyValue(mixed $value, array $config, string $id) : void
    {
        if (($config['empty'] ?? TRUE) === FALSE)
        {
            static::isTrue(
                $value !== NULL && $value !== '' && $value !== [],
                dgettext('validatable', '`{0}` no puede tener un valor vacío'),
                $id
            );
        }
    }

    /**
     * Verifica que la longitud del texto no sobrepase el máximo permitido.
     *
     * @param string $value  Valor a validar.
     * @param array  $config Configuración de validación
     * @param string $id     Identificador del valor que se va a validar.
     *
     * @return void
     */
    public static function strMaxLength(string $value, array $config, string $id) : void
    {
        if (array_key_exists('maxLength', $config))
        {
            static::strLengthLessOrEqual(
                $value,
                $config['maxLength'],
                dgettext('validatable', '`{0}` debe tener una longitud menor o igual a {4}'),
                $id
            );
        }
    }

    /**
     * Verifica que la longitud del texto tenga al menos el mínimo permitido.
     *
     * @param string $value  Valor a validar.
     * @param array  $config Configuración de validación
     * @param string $id     Identificador del valor que se va a validar.
     *
     * @return void
     */
    public static function strMinLength(string $value, array $config, string $id) : void
    {
        if (array_key_exists('minLength', $config))
        {
            static::strLengthGreaterOrEqual(
                $value,
                $config['minLength'],
                dgettext('validatable', '`{0}` debe tener una longitud mayor o igual a {4}'),
                $id
            );
        }
    }
}