# jf/validatable

Validación de objetos y valores.

## Instalación

### Composer

Este proyecto usa como gestor de dependencias [Composer](https://getcomposer.org) el cual puede ser instalado siguiendo
las instrucciones especificadas en la [documentación](https://getcomposer.org/doc/00-intro.md) oficial del proyecto.

Para instalar el paquete `jf/validatable` usando este manejador de paquetes se debe ejecutar:

```sh
composer require jf/validatable
```

#### Dependencias

Cuando el proyecto es instalado, adicionalmente se instalan las siguientes dependencias:

| Paquete       | Versión |
|:--------------|:--------|
| jf/serializer | ^1.0    |

### Control de versiones

Este proyecto puede ser instalado usando `git`. Primero se debe clonar el proyecto y luego instalar las dependencias:

```sh
git clone https://www.gitlab.com/jfphp/jfValidatable.git
cd jfValidatable
composer install
```

## Archivos disponibles

### Clases

| Nombre                                                                  | Descripción                                                                               |
|:------------------------------------------------------------------------|:------------------------------------------------------------------------------------------|
| [jf\Validatable\Assert](src/Assert.php)                                 | Aserciones para la validación de los valores.                                             |
| [jf\Validatable\Attribute\Validatable](src/Attribute/Validatable.php)   | Atributo para marcar las propiedades validables.                                          |
| [jf\Validatable\Serializer\Getters](src/Serializer/Getters.php)         | Serializador que toma en cuenta la existencia de getters para las propiedades protegidas. |
| [jf\Validatable\Serializer\Scalar](src/Serializer/Scalar.php)           | Serializa un objeto dejando solamente los valores escalares o los listados de escalares.  |
| [jf\Validatable\Serializer\Validatable](src/Serializer/Validatable.php) | Serializador para objetos que implementan la interfaz `jf\Validatable\IValidatable`.      |
| [jf\Validatable\Validator\Validator](src/Validator/Validator.php)       | Valida las propiedadesde un objeto que requieren validación.                              |

### Interfaces

| Nombre                                                              | Descripción                                                         |
|:--------------------------------------------------------------------|:--------------------------------------------------------------------|
| [jf\Validatable\IValidatable](src/IValidatable.php)                 | Interfaz para la validación de propiedades de clases y sus valores. |
| [jf\Validatable\Validator\IValidator](src/Validator/IValidator.php) | Interfaz para la validación de valores.                             |

### Traits

| Nombre                                              | Descripción                                                              |
|:----------------------------------------------------|:-------------------------------------------------------------------------|
| [jf\Validatable\TValidatable](src/TValidatable.php) | Gestiona la obtención de la configuración de las propiedades validables. |
